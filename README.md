# scripts

Handy scripts, add the path to the repo on PATH var and you can use

## currently implemented

### Angular generator

Use 'ang help' for list of options
- generate anguar project with gulp tasks already 'ang new projectName'
- generate angular module using camelCase 'ang module sampleName'
- generate angular service using camelCase 'ang service sampleName'
- generate angular factory using camelCase 'ang factory sampleName'
- generate angular value using camelCase 'ang value sampleName'
- generate angular constant using camelCase 'ang constant sampleName'
- generate angular filter using camelCase 'ang filter sampleName'
- remove module using 'ang rm moduleName'
- remove item like filter etc using command 'ang rm sampleName filter' obvs replace filter for type
- view help docs with 'ang help'

for all of the above you can make the items as a stand alone module (required by parent app) or as a item inside an existing module my choosing the module to insert it into (follow on screen instructions)


## whats comming up

Other generators like angular ionic, ionic components etc

[![Stories in Ready](https://badge.waffle.io/garethfuller/scripts.png?label=ready&title=Ready)](http://waffle.io/garethfuller/scripts)
